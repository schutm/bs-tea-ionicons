const fs = require("fs");
const util = require("util");

const ionicons = require("ionicons/icons");

const { toCamelCase, toSnakeCase } = require("js-convert-case");
const xmlParser = require("xml-js");

const svgo = new (require("svgo/lib/svgo"))({
  plugins: [
    { removeUselessStrokeAndFill: false },
    { removeXMLNS: true },
    { removeAttrs: { attrs: "data-.*" } },
    { convertStyleToAttrs: true },
  ],
});

function decodeSVGDatauri(str) {
  var regexp = /data:image\/svg\+xml(;(?:charset=)?[^;,]*)?(;base64)?,(.*)/;
  var match = regexp.exec(str);

  // plain string
  if (!match) return str;

  var data = match[3];

  if (match[2]) {
    // base64
    str = new Buffer(data, "base64").toString("utf8");
  } else if (data.charAt(0) === "%") {
    // URI encoded
    str = decodeURIComponent(data);
  } else if (data.charAt(0) === "<") {
    // unencoded
    str = data;
  }

  return str;
}

function removeClasses(el) {
  (el.attributes["class"] || "").split(" ").forEach((item, index) => {
    switch (item) {
      case "ionicon":
        el.attributes["stroke"] = el.attributes["stroke"] || "currentColor";
        el.attributes["fill"] = el.attributes["fill"] || "currentColor";
        break;
      case "ionicon-fill-none":
        el.attributes["fill"] = el.attributes["fill"] || "none";
        break;
      case "ionicon-stroke-width":
        el.attributes["stroke-width"] = el.attributes["stroke-width"] || "32px";
        break;
    }
  });
  delete el.attributes["class"];

  (el.elements || []).forEach((el) => removeClasses(el));
}

function ocamlize(functionName, xmlJs) {
  function dekeyword(word) {
    const keywords = [
      "and",
      "as",
      "assert",
      "asr",
      "begin",
      "class",
      "constraint",
      "do",
      "done",
      "downto",
      "else",
      "end",
      "exception",
      "external",
      "false",
      "for",
      "fun",
      "function",
      "functor",
      "if",
      "in",
      "include",
      "inherit",
      "initializer",
      "land",
      "lazy",
      "let",
      "lor",
      "lsl",
      "lsr",
      "lxor",
      "match",
      "method",
      "mod",
      "module",
      "mutable",
      "new",
      "nonrec",
      "object",
      "of",
      "open",
      "or",
      "private",
      "rec",
      "sig",
      "struct",
      "then",
      "to",
      "true",
      "try",
      "type",
      "val",
      "virtual",
      "when",
      "while",
      "with",
    ];

    return keywords.includes(word) ? word + "'" : word;
  }

  function mapAttributes(attributes) {
    return Object.keys(attributes || {}).map(
      (key) => `A.${dekeyword(toCamelCase(key))} "${attributes[key]}"`
    );
  }

  function mapElement(element) {
    const name = element.name;
    const attributes = mapAttributes(element.attributes);
    const elements = (element.elements || []).map((element) =>
      mapElement(element)
    );

    return `E.${name} [ ${attributes.join("; ")} ] [ ${elements.join("; ")} ]`;
  }

  const svgElement = xmlJs.elements[0];
  const svgAttributes = mapAttributes(svgElement.attributes);
  const vdom = svgElement.elements.map(mapElement);

  return `let ${dekeyword(toSnakeCase(functionName))} properties =
  let module E = Tea.Svg in
  let module A = Tea.Svg.Attributes in
  E.svg ([ ${svgAttributes.join("; ")} ] @ properties) [ ${vdom.join("; ")} ]`;
}

(async () => {
  const icons = await Promise.all(
    Object.keys(ionicons).map((name) =>
      (async () => {
        const data = decodeSVGDatauri(ionicons[name]);

        return await svgo.optimize(data).then(({ data, info }) => {
          let xmlJs = xmlParser.xml2js(data);
          removeClasses(xmlJs.elements[0]);

          const ocaml = ocamlize(name, xmlJs);

          return {
            data,
            xmlJs,
            ocaml,
          };
        });
      })()
    )
  );

  fs.mkdir("./src", {}, () => {
    fs.writeFile(
      "./src/ionicons.ml",
      icons.map((icon) => icon.ocaml).join("\n\n"),
      (err) => {
        if (err) throw err;
      }
    );
  });
})();
