#!/bin/bash

bail() {
    echo -e $1
    exit
}

which apt > /dev/null 2>&1 || bail "Not a debian based system. Please run:\n  vagrant up && vagrant ssh"

BASE=$(dirname ${0})
PROJECT=$( cd "${BASE}/.." && basename ${PWD} )
PROJECT_DIR=$( cd "${BASE}/.." && echo ${PWD} )
SOURCE_DIR="${SOURCE_DIR-${PROJECT_DIR}/sources}"
BUILD_DIR="${BUILD_DIR-${PROJECT_DIR}/builds}"

provision () {
  echo -n "Provisioning $1... "
}

provisioning_successful () {
  echo "done"
  return 0
}

provisioning_not_required () {
  echo "already installed"
  return 1
}

provisioning_failed () {
  echo "failed" >&2
  return 1
}

is_virtualbox () {
  sudo -s dmidecode -s system-product-name | grep -qw VirtualBox
}

for provisioner in `ls -v ${SOURCE_DIR}/provisioners/*.provisioner`; do
  . ${provisioner}
done
