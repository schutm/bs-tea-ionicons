project     = ENV.fetch('PROJECT', File.basename(File.dirname(__FILE__)))
base_dir    = File.expand_path(File.dirname(__FILE__) + '/../..')

$HOST = {
	'base_dir'    => ENV.fetch('BASEDIR', base_dir),
	'source_dir'  => ENV.fetch('SOURCEDIR', File.dirname(__FILE__)),
	'output_dir'  => ENV.fetch('OUTPUTDIR', "#{base_dir}/Outputs/#{project}")
}

$GUEST = {
	'base_dir'    => '/project',
	'source_dir'  => '/project/sources',
  'env'         => {
  }
}

def ensure_dir target
  unless File.directory?(target)
    FileUtils.mkdir_p(target)
  end
end

def provision root
  Vagrant.configure(2) do |config|
    Dir.glob("#{root}/**/*.provisioner").sort.each do |f|
      /^(?<name>[^.]*)(\.(?<flags>[ua]*))?\.provisioner/ =~ f
      env = $GUEST['env'].merge({ "SOURCEDIR" => $GUEST['source_dir'] })
      privileged = (flags == nil) || not(flags.include? 'u')
      run = ((flags != nil) && (flags.include? 'a')) ? "always" : "once"

      puts "Including '#{f}' (privileged: #{privileged}; run: #{run})"
      config.vm.provision "shell", name: f, path: f, env: env, privileged: privileged, run: run
    end
  end
end

def provision_environment
  puts "Setting environment variables"
  Vagrant.configure(2) do |config|
    config.vm.provision "shell", run: "always", inline: %{
tee "/etc/profile.d/myvars.sh" <<EOF
#{ $GUEST['env'].map { |k,v| "export #{k}=\"#{v}\"" }.join("\n") }
EOF
}
  end
end

ensure_dir($HOST['output_dir'])

Vagrant.configure(2) do |config|
  # Box to work with
  config.vm.box = "debian/contrib-buster64"
  config.vm.box_check_update = false

  # Network

  # Shared folders.
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder $HOST['source_dir'], $GUEST['source_dir']

  # Make sure we don't get non-interactive shell problems
  config.ssh.shell = "bash"

  # Set owner of shared folder.
  config.vm.provision "shell", name: "Fix owner settings of project directory", inline: "chown vagrant:vagrant #{$GUEST['base_dir']}"

  # Set the environment variables
  config.vm.provision "shell", name: "Set environment variables", run: "always", inline: %{
tee "/etc/profile.d/myvars.sh" <<EOF
#{ $GUEST['env'].map { |k,v| "export #{k}=\"#{v}\"" }.join("\n") }
EOF
}

  # Provision the box
  config.vm.provision "shell", name: "Provision environment: setup.sh", inline: "/project/sources/setup.sh", run: "always", privileged: false
end
